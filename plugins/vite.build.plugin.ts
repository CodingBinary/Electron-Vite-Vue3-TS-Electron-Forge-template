// 项目打包时候的自定义插件

import { Plugin } from "vite";

// 通过 spawn 的方式执行 electron-forge 的打包命令
import { ChildProcess, spawn } from "child_process";
import os from 'os'
import fs from 'fs'
import * as path from 'path';

// 引入esbuild,把 electron 的 ts 打包成 js 
import { BuildResult, buildSync } from "esbuild";


// 递归拷贝文件夹及其内容  
function copyDirSync(src: string, dest: string) {
    // 确保目标目录存在  
    if (!fs.existsSync(dest)) {
        fs.mkdirSync(dest, { recursive: true });
    }

    fs.readdirSync(src).forEach(file => {
        const srcFile = path.join(src, file);
        const destFile = path.join(dest, file);

        // 递归拷贝子目录  
        if (fs.lstatSync(srcFile).isDirectory()) {
            copyDirSync(srcFile, destFile);
        } else {
            // 拷贝文件  
            fs.copyFileSync(srcFile, destFile);
        }
    });
}


// 打包electron.ts
const electronBuild2Js = () => {
    // 每次打包前先删除编译后目录，再进行编译
    let targetExistFlag = fs.existsSync('electronbuild')
    if (targetExistFlag) {
        console.log('electronbuild 目录存在，执行删除')
        fs.rmSync('electronbuild', { recursive: true })
    } else {
        console.log('electronbuild 目录不存在，无需删除')
    }

    // 编译打包electron 入口ts文件
    let buildRes: BuildResult = buildSync({
        entryPoints: ['electron/**/*.ts'],
        bundle: true,
        outdir: 'electronbuild',
        platform: 'node',
        target: 'node20',
        external: ['electron']
    })
    console.log('编译 electron ts 文件结果 ： ', buildRes)

    
    // 拷贝electron编译后文件按到build目录
    const sourceDir = path.join(__dirname, '../electronbuild');
    const destinationDir = path.join(__dirname, '../build');

    copyDirSync(sourceDir, destinationDir);
    console.log('electronbuild文件夹及其内容拷贝完成！');

}

// 思路 ： 先等vite 打完包，然后再执行 electron的打包动作
// 因为，electron 打包是需要用到 vue 打包之后的 index.html 文件的
export const ElectronBuildPlugin = (): Plugin => {
    return {
        name: 'electron-build-plugin',

        // buildStart() {
        //     console.log('vite-prod 开始编译vue对象 ： 先重新编译 electron 的 ts ')

           
            
        // },

        closeBundle() {
            console.log('vite-vue 打包完成')

             // 执行 electron 的 编译动作
            electronBuild2Js();

            // 先把之前的打包的文件删除
            let dirFlag = fs.existsSync('out')
            if (dirFlag) {
                console.log('plugins-build : out目录 存在，先删除')
                fs.rmSync('out', { recursive: true })
            } else {
                console.log('plugins-build : out 目录 不存在')
            }

            // 下面执行命令进行electron的打包
            const platform = os.platform();

            if (platform === 'win32') {
                console.log('当前运行环境是 Windows');
                // windows 上需要执行这种方式
                let buildChildProcess = spawn('npm.cmd', ['run', 'make'], { stdio: 'inherit' }) as ChildProcess

            } else if (platform === 'darwin') {
                console.log('当前运行环境是 Mac');
                // Mac上可以执行这种方式
                // let buildChildProcess = spawn('electron-forge',['make'],{stdio: 'inherit'}) as ChildProcess
                let buildChildProcess = spawn('npm', ['run', 'make'], { stdio: 'inherit' }) as ChildProcess
            } else if (platform === 'linux') {
                console.log('当前运行环境是 linux');
                let buildChildProcess = spawn('npm', ['run', 'make'], { stdio: 'inherit' }) as ChildProcess
            } else {
                console.log('其他平台 : ', platform, '【暂不支持打包】');
            }

        },
    }
}

