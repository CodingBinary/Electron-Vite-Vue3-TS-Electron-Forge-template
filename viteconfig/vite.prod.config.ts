// vite 生产环境变量配置

import {defineConfig} from 'vite'

console.log('load prod-config...')

export default defineConfig({
    // 配置打包属性
    base: './',
    build: {
        outDir: 'build'
    }
})