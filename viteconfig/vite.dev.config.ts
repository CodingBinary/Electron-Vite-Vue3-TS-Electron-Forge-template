// vite 开发环境变量配置

import {defineConfig} from 'vite'

console.log('load dev-config...')

export default defineConfig({
    // 指定服务端口
    server: {
        port: 5179
    }
})