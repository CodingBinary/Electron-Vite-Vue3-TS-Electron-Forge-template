// vite 基础通用配置

import { fileURLToPath, URL } from 'node:url'
import vue from '@vitejs/plugin-vue'

import {defineConfig} from 'vite'
import {ElectronDevPlugin} from '../plugins/vite.dev.plugin'
import { ElectronBuildPlugin } from '../plugins/vite.build.plugin'

console.log('load base-config...')

export default defineConfig({
    plugins: [
        vue(),
        ElectronDevPlugin(),
        ElectronBuildPlugin(),
    ],

    // 指定参数配置的文件目录，加载环境用
    envDir: 'environmentconfig',

    resolve: {
        alias: {
            '@': fileURLToPath(new URL('../src', import.meta.url))
        }
    },
})