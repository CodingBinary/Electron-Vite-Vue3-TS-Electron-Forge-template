/**
 * electron主进程
 */
import { app, BrowserWindow } from 'electron'
import path from 'path'
import fs from 'fs'
const log = require('electron-log');


// 创建主窗口
const createWindow = () => {
    const window = new BrowserWindow({
        width: 800,
        height: 600
    })

    // 根据是否存在开发环境服务器路径，加载不同内容
    let devUrl = process.argv[2]
    if (devUrl) {
        window.loadURL(devUrl)
    } else {
        // window.webContents.openDevTools()
        // window.loadFile('build/index.html')
        window.loadFile(path.resolve(__dirname,'index.html'))
    }
    console.log('prod file path====== : ', path.resolve(__dirname, 'index.html'))



    // 获取当前 Electron 应用的运行路径  
    const appPath = app.getAppPath() // 这通常是应用的 package.json 所在的位置  
    log.info("appPath:", appPath)
    // 或者，如果你想要的是 main.js 所在的目录，可以使用 __dirname  
    const mainDirPath = __dirname
    log.info("__dirname:", __dirname)
    log.info("======================================")

}






// 应用准备就绪，加载窗口
app.whenReady().then(() => {
    console.log('electronMain.js : ready')
    createWindow()
    // mac上默认保留一个窗口
    app.on('activate', () => {
        if (BrowserWindow.getAllWindows().length > 0) createWindow()

    })
    console.log('--- app ready ---')
})

// 关闭所有窗口：程序退出
// 对于 macOS，当所有窗口都关闭时，应用通常不会退出，而是保持在 Dock 中。用户可以通过点击 Dock 图标来重新打开窗口。这就是macOS，这里没有调用 app.quit()
app.on('window-all-closed', () => {
    if (process.platform != 'darwin') app.quit()
})








