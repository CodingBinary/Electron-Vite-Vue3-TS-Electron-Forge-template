

- [Electron vite vue3 TS Electron ElectronForge 开发项目搭建](#electron-vite-vue3-ts-electron-electronforge-开发项目搭建)
- [1.说明](#1说明)
- [2.使用](#2使用)
- [3.项目介绍](#3项目介绍)
- [4. 开发环境搭建说明](#4-开发环境搭建说明)
  - [4.1 主要流程](#41-主要流程)

# Electron vite vue3 TS Electron ElectronForge 开发项目搭建
# 1.说明
 - 开发幻境一键打包
 - 支持electron 和 vue 热更新
 - 一键打包

# 2.使用
- 安装： npm install
- 开发运行： npm run dev
- 打包： npm run build

# 3.项目介绍
- src: vue工程, 结构保持vue一致即可
- electron:  electron 文件，包含主进程入口和其他ts文件， 及预加载文件等
  - electronMain: 主进程文件
- plugins：自定义的开发环境和生产环境vite插件
- environmentconfig： 项目的相关配置，环境变量等
- viteconfig： vite 配置，基础，开发，生产环境配置，最后加载用来确定和区分环境
- electronbuild： 运行打包时中间目录，为了暂存electron ts 编译后的js, 完成后在项目运行时会将其复制到 build 目录下
- out： forge 打包后输出目录
  
# 4. 开发环境搭建说明
## 4.1 主要流程
    - 利用vite钩子函数，在vue项目启动完成后使用子进程调起electron服务，同时将vite服务地址传到electron主进程文件中，electron加载服务地址实现一键启动。
    - electron 不支持ts，使用ts的话需要在electron启动前编译
    - 监听 electron下的文件变化实现electron动态更新
    - 打包也相同，等vue打包完成后进行electronMain.ts编译, 调起electron-forge 打包
