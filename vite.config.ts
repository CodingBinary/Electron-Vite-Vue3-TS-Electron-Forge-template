// vite 根配置，根据不同的运行参数，读取不同配置文件

import { defineConfig, loadEnv } from 'vite'

import { fileURLToPath, URL } from 'node:url'

// 引入三个环境的配置文件
import ViteBaseConfig from './viteconfig/vite.base.config'
import ViteProdConfig from './viteconfig/vite.prod.config'
import ViteDevConfig from './viteconfig/vite.dev.config'
import path from "path"

// 合并环境配置
const envResolver = {
  'build': () => {
    console.log("加载生产环境 : ")
    // 解构的语法
    return ({ ...ViteBaseConfig, ...ViteProdConfig })
  },
  "serve": () => {
    console.log("加载开发环境 : ")
    // 另一种写法
    return Object.assign({}, ViteBaseConfig, ViteDevConfig)
  }
}


// 根据参数command值来返回不同配置文件
export default defineConfig(({ command, mode }) => {
  console.log("vite start : command : ", command)
  console.log("vite start : mode : ", mode)
  console.log("process.cwd() : ", process.cwd())
  console.log("__dirname: ", __dirname)
  console.log("envdir ： ", path.resolve(process.cwd(),'./environmentconfig'))

  // 加载不同环境下参数
  const envParams = loadEnv(mode, path.resolve(process.cwd()))

  // // 根据不同的环境使用不同的配置文件,合并对象
  return envResolver[command]()
})
